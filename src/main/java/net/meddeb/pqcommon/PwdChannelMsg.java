package net.meddeb.pqcommon;
/*------------------------------------------------------------------------------------
Common module of pqMessenger, the messaging middleware for pqChecker and JMS service
Copyright (C) 2018, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

public enum PwdChannelMsg {
	READ_REQUEST("PQ-REQ-Read"), 
	WRITE_REQUEST("PQ-REQ-Write"), 
	READ_RESPONSE("PQ-RES-Read"), 
	WRITE_RESPONSE("PQ-RES-Write"),
	BROADCAST_PWD("PQ-BRO-Pwd"); 
	
	private final String name;
	
	public static PwdChannelMsg value(String strName){
		if (strName.equals(PwdChannelMsg.READ_REQUEST.strName())){
			return READ_REQUEST;
		} else if (strName.equals(PwdChannelMsg.WRITE_REQUEST.strName())){
			return WRITE_REQUEST;
		} else if (strName.equals(PwdChannelMsg.READ_RESPONSE.strName())){
			return READ_RESPONSE;
		} else if (strName.equals(PwdChannelMsg.WRITE_RESPONSE.strName())){
			return WRITE_RESPONSE;
		} else if (strName.equals(PwdChannelMsg.BROADCAST_PWD.strName())){
			return BROADCAST_PWD;
		} else return null;
	}
	
	public static String[] getStrArray(){
		String[] rslt = {PwdChannelMsg.READ_REQUEST.strName(), PwdChannelMsg.WRITE_REQUEST.strName(), 
										 PwdChannelMsg.READ_RESPONSE.strName(),PwdChannelMsg.WRITE_RESPONSE.strName(),
										 PwdChannelMsg.BROADCAST_PWD.strName()};
		return rslt;
	}
	
	private PwdChannelMsg(String name){
		this.name = name;
	}
	
	public boolean equalsName(String otherName){
		return (otherName == null)? false: this.name.equals(otherName);
	}
	
	public String strName(){
		return this.name;
	}

}
