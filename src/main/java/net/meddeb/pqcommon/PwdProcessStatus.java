package net.meddeb.pqcommon;
/*------------------------------------------------------------------------------------
Common module of pqMessenger, the messaging middleware for pqChecker and JMS service
Copyright (C) 2018, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

public enum PwdProcessStatus {
	SUCCESS("Success"),
	FAIL("Fail");
	
	public static PwdProcessStatus value(String strName){
		if (strName.equals(PwdProcessStatus.SUCCESS.strName())){
			return SUCCESS;
		} else if (strName.equals(PwdProcessStatus.FAIL.strName())){
			return FAIL;
		} else return null;
	}
	
	private final String name;
	
	private PwdProcessStatus(String name){
		this.name = name;
	}
	
	public boolean equalsName(String otherName){
		return (otherName == null)? false: this.name.equals(otherName);
	}
	
	public String strName(){
		return this.name;
	}
}
