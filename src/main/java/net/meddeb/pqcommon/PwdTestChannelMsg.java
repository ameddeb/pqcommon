package net.meddeb.pqcommon;
/*------------------------------------------------------------------------------------
Common module of pqMessenger, the messaging middleware for pqChecker and JMS service
Copyright (C) 2018, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

public enum PwdTestChannelMsg {
		PWDTEST_REQUEST("PWDREQ-Test"),
		PWDTEST_RESPONSE("PWDRES-Test");
		
		private final String name;
		
		public static PwdTestChannelMsg value(String sn){
			if (sn.equalsIgnoreCase(PwdTestChannelMsg.PWDTEST_REQUEST.strName())){
				return PWDTEST_REQUEST;
			} else if (sn.equalsIgnoreCase(PwdTestChannelMsg.PWDTEST_RESPONSE.strName())){
				return PWDTEST_RESPONSE;
			} else return null;
		}
		
		public static String[] getStrArray(){
			String[] rslt = {PwdTestChannelMsg.PWDTEST_REQUEST.strName(), PwdTestChannelMsg.PWDTEST_RESPONSE.strName()};
			return rslt;
		}
		
		private PwdTestChannelMsg(String name){
			this.name = name;
		}
		
		public boolean equalsName(String otherName){
			return (otherName == null)? false: name.equals(otherName);
		}
		
		public String strName(){
			return this.name;
		}

}
