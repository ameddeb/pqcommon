package net.meddeb.pqcommon;
/*------------------------------------------------------------------------------------
Common module of pqMessenger, the messaging middleware for pqChecker and JMS service
Copyright (C) 2018, Abdelhamid MEDDEB (abdelhamid@meddeb.net)  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
---------------------------------------------------------------------*/

public enum PwdChannelProperties {
	TYPE("msgType"), 
	SENDERID("senderID"),
	QSFORMAT("ULSD"), 
	CHANNELID("PwdChannel"); 
	
	private final String name;
	
	public static PwdChannelProperties value(String strName){
		if (strName.equals(PwdChannelProperties.TYPE.strName())){
			return TYPE;
		} else if (strName.equals(PwdChannelProperties.SENDERID.strName())){
			return SENDERID;
		} else if (strName.equals(PwdChannelProperties.QSFORMAT.strName())){
			return QSFORMAT;
		} else if (strName.equals(PwdChannelProperties.CHANNELID.strName())){
			return CHANNELID;
		} else return null;
	}
	
	public static String[] getStrArray(){
		String[] rslt = {PwdChannelProperties.TYPE.strName(), PwdChannelProperties.SENDERID.strName(), 
				PwdChannelProperties.QSFORMAT.strName(), PwdChannelProperties.CHANNELID.strName()};
		return rslt;
	}
	
	private PwdChannelProperties(String name){
		this.name = name;
	}
	
	public boolean equalsName(String otherName){
		return (otherName == null)? false: this.name.equals(otherName);
	}
	
	public String strName(){
		return this.name;
	}


}
